import pretty_midi
import numpy as np
# For plotting
import mir_eval.display
import librosa.display
import matplotlib.pyplot as plt

from numpy.random import randint as rndi
from numpy.random import random as rnd



# Construct a PrettyMIDI object.
# We'll specify that it will have a tempo of 80bpm.
pm = pretty_midi.PrettyMIDI(initial_tempo=80)


# The instruments list from our PrettyMIDI instance starts empty
# print(pm.instruments)

np.random.seed(12)


# inst = pretty_midi.Instrument(program=42, is_drum=False, name='my cello')
inst = pretty_midi.Instrument(program=15, is_drum=False, name='my cello')
pm.instruments.append(inst)




nodelen=0.4
nodeoverlay=0.1
nodevelocity=100
minpitch=50
maxpitch=70
nodenum=10
setsize=8
setnum=3
setrep=5
set2size=8
set2num=3
set2rep=5



nodes=[int(x) for x in np.arange(minpitch,maxpitch*1.001,(maxpitch-minpitch)/nodenum)]

nodes=[{"inn":x,"out":x,"q":x,"multi":False,"p":1} for x in nodes]

# print(nodes)

def probabl(inn,out):
  if inn["inn"]==out["out"]:return 1
  if inn["q"]==out["q"]:return 1
  return 100.0/np.abs(inn["inn"]-out["out"])

def setprobs(nodes,before):
  for i in range(len(nodes)):
    nodes[i]["p"]=probabl(before,nodes[i])
  return nodes

def dice(nodes):
  cou=np.sum([x["p"] for x in nodes])
  roll=rnd()*cou
  for n in nodes:
    if roll<n["p"]:return n
    roll-=n["p"]
  return nodes[rndi(len(nodes))]

def flatten(q):
  ret=[]
  for ac in q:
    if ac["multi"]:
      for sq in ac["q"]:ret.append(sq)
    else:
      ret.append(ac["q"])
  return ret

def daddy(q):
  return {"inn":q[0],"out":q[-1],"q":q,"multi":True,"p":1}

def genset(nodes,n=8):
  rel=[]
  ac=nodes[rndi(len(nodes))]
  rel.append(ac)
  for i in range(n-1):
    nodes=setprobs(nodes,ac)
    ac=dice(nodes)
    rel.append(ac)
  return daddy(flatten(rel))
  


sets=[genset(nodes,n=setsize) for i in range(setnum)]
sets2=[genset(sets,n=setrep) for i in range(set2num)]



q=[]
# for n in nodes:q.append(n)
q=genset(sets2,n=set2rep)["q"]





print("generated",q)

plt.plot(q)
plt.savefig("acsong.png",format="png")
plt.close()



song=[]
pos=0.2
for pitch in q:
  song.append(pretty_midi.Note(nodevelocity,pitch,pos-nodeoverlay,pos+nodelen))
  pos+=nodelen


for note in song:
  inst.notes.append(note)



# # Let's add a few notes to our instrument
# velocity = 100
# # for pitch, start, end in zip([60, 62, 64], [0.2, 0.6, 1.0], [1.1, 1.7, 2.3]):
# for pitch, start, end in zip([40, 60, 80], [0.2, 0.6, 1.0], [1.1, 1.7, 2.3]):
    # inst.notes.append(pretty_midi.Note(velocity, pitch, start, end))
# print(inst.notes)

# q=pm.synthesize(fs=16000)
q=pm.fluidsynth(fs=16000)






# Use the sounddevice module
# http://python-sounddevice.readthedocs.io/en/0.3.10/

import sounddevice as sd
import time

# # Samples per second
sps = 44100
sps=16000

# # Frequency / pitch
# freq_hz = 440.0

# # Duration
# duration_s = 5.0

# # Attenuation so the sound is reasonable
# atten = 0.3

# # NumpPy magic to calculate the waveform
# each_sample_number = np.arange(duration_s * sps)
# waveform = np.sin(2 * np.pi * each_sample_number * freq_hz / sps)
# waveform_quiet = waveform * atten


duration_s=len(q)/sps

np.savez_compressed("data",q=q)


# Play the waveform out the speakers
sd.play(q, sps)
time.sleep(duration_s)
sd.stop()





